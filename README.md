# Video Modal Pop-up Project 

**Created an HTML page of basic version where it contains:**

- Avatar icon
- Name and Created by
- Details 
- Youtube image with link for Modal Pop-up 
  - When Youtube image is cliked it show an pop-up for video when clicked video gets played
- Modal Pop-up 
  - For playing video using iframe also close button is included. 
  - When close icon is clicked the pop-up disappers.
- Like button 
  - When cliked like icon background color toggles between blue and white color
- Delete button 
  - When clicked the entire content in the page gets disappers.
 
